package de.craften.plugins.textcleaner;

import java.util.Collection;

public interface BadwordsProvider {
    public Collection<Badword> getBadwords() throws BadwordsProviderException;
}
