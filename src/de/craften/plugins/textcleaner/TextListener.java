package de.craften.plugins.textcleaner;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;

public class TextListener implements Listener {
    private static final String REMOVED_STRING = "~ removed ~";

    @EventHandler
    public void onEditBook(PlayerEditBookEvent e) {
        BookMeta book = e.getNewBookMeta();
        boolean isCensored = false;
        List<String> pages = book.getPages();
        List<String> newPages = new ArrayList<String>();
        for (String page : pages) {
            MatchResult m = TextCleanerPlugin.instance.getBadwordChecker().checkString(page);
            if (m == null)
                newPages.add(page);
            else {
                newPages.add(ChatColor.DARK_RED + REMOVED_STRING);
                isCensored = true;
            }
        }
        book.setPages(newPages);
        e.setNewBookMeta(book);
        if (isCensored)
            e.getPlayer().sendMessage(ChatColor.DARK_RED + "We removed some pages with badwords from the book.");
    }

    @EventHandler
    public void onEditSign(SignChangeEvent e) {
        boolean isBad = false;
        String[] lines = e.getLines();
        for (String line : lines)
            if (TextCleanerPlugin.instance.getBadwordChecker().checkString(line) != null) {
                isBad = true;
                break;
            }

        if (isBad) {
            e.getPlayer().getWorld().createExplosion(e.getBlock().getLocation().add(0.5, .75, 0.5), 0);
            e.getPlayer().sendMessage(ChatColor.DARK_RED + "The sign was removed because of badwords!");
            e.setCancelled(true);
            e.getBlock().breakNaturally();
        }
    }

    @EventHandler
    public void onChatMessage(AsyncPlayerChatEvent e) {
        String message = e.getMessage().trim();
        String messageLower = message.toLowerCase();

        double diversity = TextCleanerPlugin.instance.getSpamFilter().getDiversity(e.getPlayer().getName(), messageLower);
        if (diversity < TextCleanerPlugin.instance.getConfig().getDouble("filter.repeatFactor")) {
            e.getPlayer().sendMessage(ChatColor.DARK_RED + "Please don't repeat yourself!");
            e.setCancelled(true);
            return;
        }

        int variety = SpamFilter.getLongestEqualLettersSequenceLength(messageLower);
        if (variety > TextCleanerPlugin.instance.getConfig().getDouble("filter.maxEqualLettersSequence")) {
            e.getPlayer().sendMessage(ChatColor.DARK_RED + "Too maaaany eeeequaaal letters!");
            e.setCancelled(true);
            return;
        }

        if (e.getMessage().length() > TextCleanerPlugin.instance.getConfig().getInt("filter.ignoreCapslockBelow")) {
            double capslock = SpamFilter.getCapslockPercentage(message);
            if (capslock > TextCleanerPlugin.instance.getConfig().getDouble("filter.capslockPercentage")) {
                e.getPlayer().sendMessage(ChatColor.DARK_RED + "PLEASE DON'T SHOUT!");
                e.setCancelled(true);
                return;
            }
        }

        MatchResult m = TextCleanerPlugin.instance.getBadwordChecker().checkString(message);
        if (m != null) {
            e.getPlayer().sendMessage(String.format(ChatColor.DARK_RED + "Message '%s' blocked! '%s' is a badword.", message, m.group()));
            e.setCancelled(true);
            TextCleanerPlugin.instance.notifyAll(e.getPlayer().getName(), m.group());
            return;
        }

        TextCleanerPlugin.instance.getSpamFilter().addMessage(e.getPlayer().getName(), messageLower);
    }
}
