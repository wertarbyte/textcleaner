package de.craften.plugins.textcleaner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collection;
import java.util.List;

public class JsonBadwordsProvider implements BadwordsProvider {
    private final String jsonUrl;

    public JsonBadwordsProvider(String jsonUrl) {
        this.jsonUrl = jsonUrl;
    }

    @Override
    public Collection<Badword> getBadwords() throws BadwordsProviderException {
        try {
            String jsonString = readUrl(this.jsonUrl);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.fromJson(jsonString, new TypeToken<List<Badword>>() {
            }.getType());
        } catch (Exception e) {
            throw new BadwordsProviderException(e);
        }
    }

    private static String readUrl(String url) throws IOException {
        BufferedReader reader = null;
        try {
            URL _url = new URL(url);
            reader = new BufferedReader(
                    new InputStreamReader(_url.openStream()));
            StringBuilder buffer = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
