package de.craften.plugins.textcleaner;

import de.craften.util.LevenshteinDistance;

import java.util.HashMap;

public class SpamFilter {
    private HashMap<String, String> lastMessages;

    public SpamFilter() {
        lastMessages = new HashMap<String, String>();
    }

    /**
     * Liefert den Unterschied zwischen der Nachricht und der von diesem Sender zuletzt gesendeten Nachricht.
     * Diese Methode ist case-sensitive!
     *
     * @param sender  Absender der Nachricht
     * @param message Nachricht
     * @return Wert zwischen 0 (kein Unterschied) und 1 (komplett verschieden)
     */
    public double getDiversity(String sender, String message) {
        String lastMessage = lastMessages.get(sender);
        if (lastMessage == null)
            return 1;

        int distance = LevenshteinDistance.getLevenshteinDistance(message, lastMessage);
        return ((double) distance) / Math.max(message.length(), lastMessage.length());
    }

    public void addMessage(String sender, String message) {
        lastMessages.put(sender, message);
    }

    /**
     * Ermittelt die Länge der längsten Sequenz aufeinander folgender gleicher Zeichen.
     * Diese Methode ist case-sensitive!
     *
     * @param message Nachricht
     * @return Maximale Anzahl gleicher aufeinander folgender Zeichen
     */
    public static int getLongestEqualLettersSequenceLength(String message) {
        int longest = 0;
        int current = 0;
        char prev = message.charAt(0);
        for (int i = 1; i < message.length(); i++) {
            if (!Character.isDigit(message.charAt(i))) {
                if (message.charAt(i) == prev) {
                    current++;
                } else {
                    if (current > longest)
                        longest = current;
                    current = 0;
                }
            }
            prev = message.charAt(i);
        }
        if (current > longest)
            longest = current;
        return longest;
    }

    /**
     * Ermittelt den Anteil an Großbuchstaben.
     *
     * @param message Nachricht
     * @return Anteil an Großbuchstaben (0..1)
     */
    public static double getCapslockPercentage(String message) {
        int capsCount = 0;
        for (int i = 0; i < message.length(); i++) {
            if (Character.isUpperCase(message.charAt(i)))
                capsCount++;
        }
        return ((double) capsCount) / message.length();
    }
}
