package de.craften.plugins.textcleaner;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class TextCleanerPlugin extends JavaPlugin {
    public static TextCleanerPlugin instance;
    public static final String PREFIX = "[TextCleaner] ";
    private static final String JSON_URL = "http://craften.de/api/badwords";
    private BadwordChecker badwordChecker;
    private SpamFilter spamFilter;

    @Override
    public void onEnable() {
        instance = this;
        badwordChecker = new BadwordChecker(new JsonBadwordsProvider(JSON_URL));
        spamFilter = new SpamFilter();

        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(new TextListener(), this);

        refreshBadwords(getServer().getConsoleSender());
    }

    public BadwordChecker getBadwordChecker() {
        return badwordChecker;
    }

    public SpamFilter getSpamFilter() {
        return spamFilter;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equals("tc")) {
            if (args.length == 1 && args[0].equals("reload") && sender.hasPermission("textcleaner.reload")) {
                refreshBadwords(sender);
                return true;
            }
        }
        return false;
    }

    public void notifyAll(String player, String badword) {
        getServer().broadcast(String.format(ChatColor.RED + player + ChatColor.RESET +
                        " wanted to write '" + ChatColor.RED + badword + ChatColor.RESET + "'."),
                "textcleaner.notify"
        );
    }

    private void refreshBadwords(final CommandSender starter) {
        getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                try {
                    badwordChecker.refreshBadwordList();
                    if (starter != null) {
                        starter.sendMessage(PREFIX + badwordChecker.getBadwordsCount() + " badwords loaded.");
                    }

                } catch (BadwordsProviderException e) {
                    getServer().getLogger().warning(PREFIX + "Could not load badwords!");
                    if (starter != null)
                        starter.sendMessage("Could not refresh the badwords!");
                }
            }
        });
    }
}
