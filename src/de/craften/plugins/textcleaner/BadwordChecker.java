package de.craften.plugins.textcleaner;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BadwordChecker {
    private BadwordsProvider badwordsProvider;
    private List<Pattern> patternList;

    public BadwordChecker(BadwordsProvider provider) {
        this.badwordsProvider = provider;
    }

    /**
     * @param s String to check
     * @return Returns the MatchResult, null if nothing was found.
     */
    public MatchResult checkString(String s) {
        for (Pattern pattern : patternList) {
            Matcher m = pattern.matcher(s);
            if (m.find()) {
                return m.toMatchResult();
            }
        }
        return null;
    }

    private String replaceCharsAt(String s, int index, String newChars) {
        char[] chars = s.toCharArray();
        for (int i = 0; i < newChars.length(); i++)
            chars[index + i] = newChars.charAt(i);
        return new String(chars);
    }

    private String getRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++)
            sb.append('#');
        return sb.toString();
    }

    public String cleanString(String s) {
        for (Pattern pattern : patternList) {
            Matcher m = pattern.matcher(s);
            while (m.find()) {
                s = replaceCharsAt(s, m.start(), getRandomString(m.end() - m.start()));
            }
        }
        return s;
    }

    public void refreshBadwordList() throws BadwordsProviderException {
        patternList = preCompile(badwordsProvider.getBadwords());
    }

    public int getBadwordsCount() {
        return patternList != null ? patternList.size() : 0;
    }

    private List<Pattern> preCompile(Iterable<Badword> badwords) {
        List<Pattern> patternList = new ArrayList<Pattern>();
        for (Badword badword : badwords) {
            try {
                patternList.add(Pattern.compile(badword.getRegex(), Pattern.CASE_INSENSITIVE));
            } catch (Exception e) {
            }
        }
        return patternList;
    }
}
