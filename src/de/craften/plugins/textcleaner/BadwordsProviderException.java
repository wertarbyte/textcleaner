package de.craften.plugins.textcleaner;

public class BadwordsProviderException extends Exception {
    public BadwordsProviderException(Exception innerException) {
        super(innerException);
    }
}
