package de.craften.plugins.textcleaner;

public class Badword {
    private String badness;
    private String id;
    private String metadata;
    private String regex;

    public String getBadness() {
        return this.badness;
    }

    public void setBadness(String badness) {
        this.badness = badness;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetadata() {
        return this.metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getRegex() {
        return this.regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }
}
